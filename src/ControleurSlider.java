import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;

public class ControleurSlider implements ChangeListener<Number>{
    private Pendu vuePendu;

    public ControleurSlider(Pendu vuePendu){
        this.vuePendu = vuePendu;
    }
    
    @Override
    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
    {   
        Double nouvelleValeur = (Double) newValue;
        this.vuePendu.augmenteText(nouvelleValeur);
        this.vuePendu.setCoef(nouvelleValeur);
        this.vuePendu.majAffichage();
    }
}
