import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public class ControleurMaxLettre implements ChangeListener<Boolean>{
        private Pendu vuePendu;
        private TextField max;

        public ControleurMaxLettre(TextField max, Pendu vuePendu){
            this.max = max;
            this.vuePendu = vuePendu;
        }
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue){
            int nbr = Integer.parseInt(this.vuePendu.minLettre());
            int nbr1 = Integer.parseInt(this.vuePendu.maxLettre());
            if (!newValue) { // si on a perdu le focus
                if(nbr>nbr1){
                    this.vuePendu.setMessageError("Veuillez entrer un max > min");
                    this.vuePendu.setRougeMin(this.max);
                }
                else{
                    this.vuePendu.viderCouleur();
                    this.vuePendu.viderErreur();
                }
        }
    }
}
