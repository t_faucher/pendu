import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;

import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une partie
 */
public class ControleurChangeJeu implements EventHandler<ActionEvent> {

    /**
     * vue du jeu
     **/
    private Pendu vuePendu;
    private DemineurGraphique demineur;
    /**
     * modèle du jeu
     */
    public ControleurChangeJeu( Pendu vuePendu) {
        this.vuePendu = vuePendu;
    }
    public ControleurChangeJeu( DemineurGraphique vueDemineur) {
        this.demineur = vueDemineur;
    }
    @Override
    public void handle(ActionEvent e){
        if(this.vuePendu != null){
            Optional<ButtonType> reponse = this.vuePendu.popUpChangeDeJeu().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){

            vuePendu.changeJeuDemineur();
            vuePendu.majAffichage();
            }
        }else{
            Optional<ButtonType> reponse = this.demineur.popUpChangeDeJeu().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){

            
            this.demineur.changeJeuPendu();
            this.demineur.maj_de_la_grille();
            }
                
        }
        
    }
}