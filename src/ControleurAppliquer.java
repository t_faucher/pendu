import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;

import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une partie
 */
public class ControleurAppliquer implements EventHandler<ActionEvent> {

    /**
     * vue du jeu
     **/
    private Pendu vuePendu;
    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * @param modelePendu modèle du jeu
     * @param p vue du jeu
     */

    public ControleurAppliquer( Pendu vuePendu, MotMystere modelePendu) {
        this.vuePendu = vuePendu;
        this.modelePendu = modelePendu;
    }
    @Override
    public void handle(ActionEvent e){
        
        String nomfic = this.vuePendu.getNomfic();
        int min = Integer.parseInt(this.vuePendu.minLettre());
        int max = Integer.parseInt(this.vuePendu.maxLettre());
        
        if(min>=3 && max>=min){
            this.vuePendu.setMin(min, max);
            try{
            this.modelePendu.changeDico(nomfic,min,max);
            this.vuePendu.viderErreur();
            this.vuePendu.modeAccueil();
            }
            catch(java.lang.NullPointerException error){
                this.vuePendu.viderCouleur();
                this.vuePendu.setMessageError("Veuillez selectionner un fichier");

            }
        }
    }
}