import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

public class ControleurRejouer implements EventHandler<ActionEvent>{
    private DemineurGraphique demineur;
    public ControleurRejouer(DemineurGraphique demineurGraphique) {
        this.demineur = demineurGraphique;
    }

    @Override
    public void handle(ActionEvent event) {
        Button bt = (Button) event.getSource();
        if(bt.getText().contains("Rejouer")){
            if(this.demineur.getPlateau().getNombreDeCasesRevelees() != 0){
                Optional<ButtonType> reponse = this.demineur.popUpPartieEnCours().showAndWait(); // on lance la fenêtre popup et on attends la réponse
                // si la réponse est oui
                if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                    this.demineur.rejouer();
                } 
            }
        }
    }

}
