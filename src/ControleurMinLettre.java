import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public class ControleurMinLettre implements ChangeListener<Boolean>{
        private Pendu vuePendu;
        private TextField min;
        public ControleurMinLettre(TextField min, Pendu vuePendu){
            this.min = min;
            this.vuePendu = vuePendu;
        }
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue){
            int nbr = Integer.parseInt(this.vuePendu.minLettre());
            if (!newValue) { // si on a perdu le focus
                if(nbr<3){
                    this.vuePendu.setMessageError("Veuillez entrer un min > 3");
                    this.vuePendu.setRougeMin(this.min);
                }
                else{
                    this.vuePendu.viderCouleur();
                    this.vuePendu.viderErreur();
                }
        }
    }
}
