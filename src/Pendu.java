import javafx.application.Application;
import java.awt.Dimension;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.ButtonBar.ButtonData ;

import java.util.Set;

import javax.swing.border.LineBorder;


import java.util.HashSet;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;

    private ToggleGroup tg;

    private BorderPane bp;

    private boolean partielance;

    private Color couleurBgTop;

    private TextField tfMin;

    private TextField tfMax;

    private String nomfic;

    private FileChooser fileChooser;

    private int taillemin;

    private int taillemax;

    private Label messageError;

    private double coef;
    private DemineurGraphique demineur;
    private Label titre;
    private Label police;
    private Label min;
    private Label max;
    private Slider sliderPolice;
    private Label nomduFichier;
    private Button Demineur;
    private Text textEx;
    private Stage affichage;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.demineur = new DemineurGraphique();
        this.coef = 1;
        this.textEx = new Text("Exemple");
        this.textEx.setFont(Font.font("Arial",32));
        this.sliderPolice = new Slider(0.75,2,1);
        this.titre = new Label("Jeu du Pendu");
        this.police = new Label("Taille de la police");
        this.min = new Label("Minimum de lettres");
        this.max  = new Label("Maximum de lettres");
        this.couleurBgTop = Color.web("#87CEFA");
        this.partielance = false;
        this.messageError = new Label("");
        this.bp = new BorderPane();
        this.taillemax = 10;
        this.Demineur = new Button("",new ImageView(new Image("bombe.png",25,25,false,false)));
        this.Demineur.setOnAction(new ControleurChangeJeu(this));
        this.taillemin = 3;
        this.nomfic = "./liste_francais.txt";    
        this.nomduFichier = new Label(this.nomfic);
        this.modelePendu = new MotMystere(this.nomfic, taillemin, taillemax, MotMystere.EXPERT, 10);
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");
        this.niveaux = Arrays.asList("Facile","Medium","Difficile","Expert");
        this.leNiveau = new Text(this.niveaux.get(this.modelePendu.getNiveau()));
        this.leNiveau.setFont(Font.font("Arial",FontWeight.NORMAL,20));
        this.dessin = new ImageView(new Image("./pendu0.png"));
        this.panelCentral = new BorderPane();
        this.chrono = new Chronometre();
        this.motCrypte = new Text(this.modelePendu.getMotCrypte());
        this.pg = new ProgressBar(this.modelePendu.getNbLettresRestantes()/this.modelePendu.getMotATrouve().length());
        this.clavier = new Clavier("ABCDEFGHIJKLMNOPQRSTUVWXYZ",new ControleurLettres(this.modelePendu, this));
        this.boutonMaison = new Button("",new ImageView(new Image("./home.png",25,25,false,false)));
        this.boutonMaison.setTooltip(new Tooltip("Accéder à l'accueil"));
        this.boutonParametres = new Button("",new ImageView(new Image("./parametres.png",25,25,false,false)));
        this.boutonParametres.setTooltip(new Tooltip("Accéder à la page paramètre"));
        this.bJouer = new Button("Lancer une partie");
        this.bJouer.setOnAction(new ControleurLancerPartie(this.modelePendu, this));
        this.boutonMaison.setOnAction(new RetourAccueil(this.modelePendu, this));
        this.boutonParametres.setOnAction(new ControleurParametre(this.modelePendu, this));
        this.tfMax = new TextField(taillemax + "");
        this.tfMin = new TextField(taillemin+ "");   
        this.fileChooser = new FileChooser();
        
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */


    public Alert popUpChangeDeJeu(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION," Êtes-vous de vouloir changer de jeu ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }

    public void setMin(int min,int max){
        this.taillemin = min;
        this.taillemax = max;
    }

    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int) dimension.getHeight();
        int width = (int) dimension.getWidth(); 
        return new  Scene(fenetre,width,height);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private BorderPane titre(){
        this.bp = new BorderPane();
        this.titre.setFont(Font.font("Arial", FontWeight.NORMAL, 32));
        Button info = new Button("", new ImageView(new Image("./info.png", 25, 25, false, false)));
        HBox hb = new HBox();
        hb.getChildren().addAll(this.Demineur,this.boutonMaison, this.boutonParametres, info);
        bp.setLeft(titre);
        bp.setRight(hb);
        bp.setBackground(new Background(new BackgroundFill(this.couleurBgTop, CornerRadii.EMPTY, Insets.EMPTY)));
        bp.setPadding(new Insets(10));
        hb.setSpacing(10);
        info.setOnAction(new ControleurInfos(this));
        info.setTooltip(new Tooltip("Voir les règles du jeu"));
        return bp;
    }

    // /**
     // * @return le panel du chronomètre
     // */
    private TitledPane leChrono(){
        TitledPane res = new TitledPane();
        VBox vb = new VBox();
        vb.getChildren().add(this.chrono);
        res.setText("Chronomètre");
        res.setCollapsible(false);
        res.setContent(vb);
        return res;
    
    }

    public void setCoef(double coef){this.coef = coef;}

    // /**
     // * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
     // *         de progression et le clavier
     // */
     private BorderPane fenetreJeu(){
         
        BorderPane interface1 = new BorderPane();
        VBox jeu = new VBox();
        this.motCrypte.setFont(Font.font("Arial",FontWeight.NORMAL,32));
        HBox hb = new HBox();
        hb.setSpacing(10);
        hb.getChildren().addAll(this.clavier);
        hb.setAlignment(Pos.CENTER);
        jeu.setAlignment(Pos.TOP_CENTER);
        jeu.getChildren().addAll(this.motCrypte, this.dessin, this.pg, hb);
        interface1.setCenter(jeu);
        VBox vboxdroite = new VBox();
        Button newMot = new Button("Nouveau mot");
        newMot.setOnAction(new ControleurLancerPartie(this.modelePendu, this));
        vboxdroite.setPadding(new Insets(30));
        vboxdroite.getChildren().addAll(this.leNiveau, this.leChrono(), newMot);
        vboxdroite.setSpacing(50);
        interface1.setRight(vboxdroite);
        return interface1;
     }

    // /**
     // * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     // */
     private VBox fenetreAccueil(){
        TitledPane tp = new TitledPane();
        this.tg = new ToggleGroup();
        VBox vb = new VBox();
        VBox vraivb = new VBox();
        RadioButton facile = new RadioButton("Facile");
        RadioButton medium = new RadioButton("Médium");
        RadioButton difficile = new RadioButton("Difficile");
        RadioButton expert = new RadioButton("Expert");
        facile.setToggleGroup(tg);
        medium.setToggleGroup(tg);
        difficile.setToggleGroup(tg);
        expert.setToggleGroup(tg);
        facile.setSelected(true);
        tp.setContent(facile);
        tp.setContent(medium);
        tp.setContent(difficile);
        tp.setContent(expert);
        vb.getChildren().addAll(facile, medium, difficile, expert);
        tp.setContent(vb);
        tp.setText("Niveau de difficulté");
        vraivb.getChildren().addAll(this.bJouer, tp);
        vraivb.setPadding(new Insets(10));
        vraivb.setSpacing(10);
        tp.setCollapsible(false);
        return vraivb;
    }

    public boolean partiePasLancee(){
        return !this.partielance;
    }


    private GridPane fenetreParametre(){
        GridPane gp = new GridPane();
        this.tfMax.focusedProperty().addListener(new ControleurMaxLettre(this.tfMax,this));
        this.tfMin.focusedProperty().addListener(new ControleurMinLettre(this.tfMin,this));
        this.police.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        
        ControleurSlider controleur = new ControleurSlider(this);
        sliderPolice.valueProperty().addListener(controleur);
        Button button = new Button("Choisir un fichier");
        button.setOnAction(e -> {
            File selectedFile = fileChooser.showOpenDialog(new Stage());
            this.nomfic = selectedFile.getName();
            this.nomduFichier.setText(this.nomfic); 
        });

        ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(this.couleurBgTop);
        Circle circle = new Circle(50);
        circle.setFill(colorPicker.getValue());
        colorPicker.setOnAction(new ControleurCouleur(this, circle, colorPicker));
        min.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        
        max.setFont(Font.font("Arial", FontWeight.NORMAL, 16));
        Button appliquer = new Button("Appliquer");
        appliquer.setOnAction(new ControleurAppliquer(this,modelePendu));
        gp.add(police, 0, 0);   
        gp.add(sliderPolice, 1, 0);  
        gp.add(this.textEx,2,0,2,1);
        gp.add(circle, 1, 1);     
        gp.add(colorPicker, 0, 1);     
        gp.add(button, 1, 4);     
        gp.add(min, 0, 2);     
        gp.add(max, 0, 3);     
        gp.add(tfMin, 1, 2);     
        gp.add(tfMax, 1, 3);     
        gp.add(appliquer, 1, 5);     
        gp.add(this.messageError, 1,6);
        gp.add(this.nomduFichier, 2,4);
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(25);
        gp.setVgap(25);
        GridPane.setHalignment(circle, HPos.CENTER);


        return gp;
    }
    public void augmenteText(double valeur){
        this.textEx.setFont(Font.font("Arial",32*valeur));
    }
    public void setMessageError(String message){
        this.messageError.setText(message);
    }

    public void setRougeMin(TextField tf1){
        tf1.setStyle("-fx-border-color: red;");
    }

    public void setRougeMax(TextField tf1){
        tf1.setStyle("-fx-border-color: red;");
    }

    public void viderErreur(){
        this.messageError.setText("");
        this.viderCouleur();
    }




    public void viderCouleur(){
        this.tfMin.setStyle("-fx-border-color: transparent;");
        this.tfMax.setStyle("-fx-border-color: transparent;");

    }
    
    public void setCouleur(String value) {
        String couleur = value.substring(2,value.toString().length()-2);
        this.bp.setStyle("-fx-background-color: #" + couleur + ";");
        this.couleurBgTop = Color.web("#" + couleur);
    }
    
    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void setPartiePasLancee(){
        this.partielance = false;
    }

    public void modeAccueil(){
        this.panelCentral.setCenter(this.fenetreAccueil());
        this.boutonMaison.setDisable(true);
        this.boutonParametres.setDisable(false);

    }
    
    public void modeJeu(){
        this.panelCentral.setCenter(this.fenetreJeu());
        this.boutonMaison.setDisable(false);
    }
    
    public void modeParametres(){
        this.panelCentral.setCenter(this.fenetreParametre());
        this.boutonParametres.setDisable(true);
        this.boutonMaison.setDisable(false);


    }
    public Toggle getToggle(){return this.tg.getSelectedToggle();}

    public FileChooser getFile(){return this.fileChooser;}

    public String minLettre(){return this.tfMin.getText();}

    public String maxLettre(){return this.tfMax.getText();}

    public String getNomfic(){return this.nomfic;}

    /** lance une partie */
    public void lancePartie(){
        this.enableTouche();
        this.modelePendu.setMotATrouver();
        this.chrono.resetTime();
        this.chrono.start();
        modeJeu();
        this.partielance = true;
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        RadioButton rd = (RadioButton) this.getToggle();
        this.leNiveau.setText(rd.getText());
        this.clavier.desactiveTouches(this.modelePendu.getLettresEssayees());
        this.pg.setProgress(1-(float) this.modelePendu.getNbLettresRestantes()/this.modelePendu.getMotATrouve().length());
        this.dessin.setImage(this.lesImages.get(this.lesImages.size()-this.modelePendu.getNbErreursRestants()-1));
        this.motCrypte.setFont(Font.font("Arial", FontWeight.NORMAL, 32*this.coef));
    }


    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        return this.chrono;
    }

    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION," La partie est en cours !\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }

    public Alert popUpInformation(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION," Le but du jeu est simple : deviner toutes les lettres\n qui doivent composer un mot, éventuellement\n avec un nombre limité de tentatives et des\n thèmes fixés à l'avance. À chaque fois que le\n joueur devine une lettre, celle-ci est affichée.\n Dans le cas contraire, le dessin d'un pendu\n se met à apparaître…", ButtonType.OK);        
        alert.setHeaderText("Règle du jeu");
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION," Tu as gagné, Félicitation !!", ButtonType.OK);        
        alert.setHeaderText("Vous avez gagné :)");
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION," Vous avez perdu \n Le mot à trouver était " + this.modelePendu.getMotATrouve(), ButtonType.OK);
        alert.setHeaderText("Vous avez perdu :C");
        return alert;
    }

    public void enableTouche() {
        Set<String> lettre = new HashSet<>();
        this.clavier.desactiveTouches(lettre);

    }

    public void desactiverTouche() {
        this.clavier.desactiverTouche();
    }



    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        this.affichage = stage;

        this.affichage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        this.affichage.setScene(this.laScene());
        this.modeAccueil();
        
        this.affichage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void changeJeuDemineur() {
        System.out.println("Salut");
        this.demineur.init();
        this.demineur.start(this.affichage);
        System.out.println("passé");
    }    
    
}
