import java.util.ArrayList;
import java.util.List;
import java.awt.Dimension;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.layout.HBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
public class DemineurGraphique extends Application {

    private Grille lePlateau;
    private Pane infos;
    private GridPane grille;
    private BorderPane jeu;
    private List<Bouton> boutons;
    private Slider slidertaille;
    private Button boutonParametres;
    private TextField ligne;
    private Integer choixligne;
    private Integer choixcolonne;
    private Integer choixnbrbombe;
    private Label erreur;
    private TextField colonne;
    private TextField nbrBombe;
    private Label labelTaille;
    private Button btrejouer;
    private Scene scene;
    private BorderPane top;
    private Button btpendu;
    private Stage affichage;
    private Pendu pendu;
    private Color couleurBgTop;

    @Override
    public void init(){
        this.btrejouer = new Button("Rejouer");
        this.btrejouer.setPrefWidth(100);
        this.btrejouer.setPrefHeight(25);
        this.btrejouer.setOnAction(new ControleurRejouer(this));
        this.erreur = new Label("");
        this.boutonParametres = new Button("",new ImageView(new Image("./parametres.png",25,25,false,false)));
        this.boutonParametres.setPadding(new Insets(10));
        this.slidertaille = new Slider(35,100,50);
        this.labelTaille = new Label((int) this.slidertaille.getValue() + "");
        this.infos = new VBox();
        this.boutons = new ArrayList<>();
        this.ligne = new TextField();
        this.top = new BorderPane();
        this.colonne = new TextField();
        this.nbrBombe = new TextField();
        try{
            int lignes = Integer.valueOf(this.getParameters().getRaw().get(0));
            int colonnes = Integer.valueOf(this.getParameters().getRaw().get(1));
            int nbBombes = Integer.valueOf(this.getParameters().getRaw().get(2)); 
            this.lePlateau = new Grille(lignes, colonnes, nbBombes);
        }
        catch(Exception e){
            if(choixligne == null)
            this.lePlateau = new Grille(5, 5, 4);
            else
                this.lePlateau = new Grille(choixligne, choixcolonne, choixnbrbombe);
        }
        this.jeu = new BorderPane();
        this.grille = new GridPane();
        this.poseBouton();
        this.couleurBgTop = Color.web("#87CEFA");
        this.btpendu = new Button("Jeu du Pendu");
        this.btpendu.setOnAction(new ControleurChangeJeu(this));
        btpendu.setPrefWidth(150);
        btpendu.setPrefHeight(25);
        this.pendu = new Pendu();
        this.titre();
        this.jeu.setTop(this.top);
        this.jeu.setCenter(this.grille);
        this.jeu.setBottom(this.poseInfo());
        this.boutonParametres.setOnAction(new ControleurParametre2(this));
        this.grille.setHgap(1);
        this.grille.setVgap(1);
        this.grille.setAlignment(Pos.CENTER);
        this.maj_de_la_grille();
        this.ligne.setText(""+this.lePlateau.getNombreDeLignes());
        this.colonne.setText(""+this.lePlateau.getNombreDeColonnes());
        this.nbrBombe.setText(""+this.lePlateau.getNombreDeBombes());
        
    }

    public void poseBouton(){
        this.boutons = new ArrayList<>();
        for (int i = 0; i<this.lePlateau.getNombreDeLignes(); i++){
            for (int j=0; j<this.lePlateau.getNombreDeColonnes(); j++){
                Case laCase = this.lePlateau.getCase(i, j);
                Bouton b = new Bouton(laCase, Double.parseDouble(this.labelTaille.getText()));

                b.setOnMouseClicked(new ControleurBouton(b, laCase, this, this.lePlateau));
                grille.add(b, i, j);
                this.boutons.add(b);
            }
        }
    

    }
    public void titre(){
        this.top.setBackground(new Background(new BackgroundFill(this.couleurBgTop, CornerRadii.EMPTY, Insets.EMPTY)));
        VBox vb = new VBox();
        Label lb = new Label("Jeu du Demineur");
        lb.setFont(Font.font("Arial", FontWeight.NORMAL,32));
        vb.getChildren().addAll(lb);
        HBox right = new HBox();
        right.getChildren().addAll( this.btpendu, this.btrejouer, this.boutonParametres);
        right.setSpacing(10);
        VBox.setMargin(right, new Insets(10));
        right.setAlignment(Pos.CENTER);
        this.top.setRight(right);
        this.top.setPadding(new Insets(10));
        this.top.setLeft(vb);
        

    }
    public BorderPane poseInfo(){
        BorderPane bd = new BorderPane();
        bd.setLeft(this.infos);

        this.maj_des_infos();
        return bd;
    }

    @Override
    public void start(Stage stage) {
        //vbox.setAlignment(Pos.TOP_CENTER);
        this.affichage = stage;
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int) dimension.getHeight();
        int width = (int) dimension.getWidth(); 
        this.scene = new Scene(this.jeu,width,height);
        this.affichage.setTitle("Demineur");
        this.affichage.setScene(scene);
        this.affichage.setResizable(true);
        this.affichage.show();
    }

    public Scene getScene(){return this.scene;}

    public void maj_de_la_grille(){
        
        for (Node b : this.grille.getChildren()){
            Bouton bb = (Bouton) b;
            bb.maj();
        }
    }

    public void desactiver(){
        for (Node b : this.grille.getChildren()){
            b.setDisable(true);
        }
    }
    
    public void maj_des_infos(){
        this.infos.getChildren().clear();
        Label label1 = new Label("Nombres de bombes : " + this.lePlateau.getNombreDeBombes());
        Label label2 = new Label("Nombres de cases marquées : " + this.lePlateau.getNombreDeCasesMarquees());
        Label label3 = new Label("Nombres de cases découvertes : " + this.lePlateau.getNombreDeCasesRevelees());
        this.infos.getChildren().addAll(label1, label2, label3);
    }
    
    public static void main(String args[]){
        Application.launch(args);
    }


    public Alert popUpPartieEnCours() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION," La partie est en cours !\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }


    public void modeParametres() {
        GridPane  para = affichageParametres();
        this.jeu.setCenter(para);

    }


    private GridPane affichageParametres() {
        this.jeu.setBottom(null);
        GridPane gp = new GridPane();
        gp.setAlignment(Pos.CENTER);
        gp.setHgap(10);
        gp.setVgap(10);
        Label lLigne = new Label("Nombre de lignes");
        Label lColonne = new Label("Nombre de colonnes");
        Label lBombes = new Label("Nombre de bombes");
        ControleurSlider2 controleur = new ControleurSlider2(this);
        HBox hbslider = new HBox();

        Label lslider = new Label("Taille d'une case");

        slidertaille.valueProperty().addListener(controleur);
        hbslider.setPrefSize(50, 50);
        Button appliquer = new Button("Appliquer");
        appliquer.setPrefWidth(100);
        appliquer.setPrefHeight(50);
        appliquer.setOnAction(new ControleurAppliquer2(this));
        
        ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(this.couleurBgTop);
        Circle circle = new Circle(50);
        circle.setFill(colorPicker.getValue());
        colorPicker.setOnAction(new ControleurCouleur2(this, circle, colorPicker));
        gp.add(circle, 1,4);
        gp.add(colorPicker, 0, 4);
        gp.add(lLigne, 0, 0);
        gp.add(this.ligne, 1, 0);
        gp.add(lColonne, 0,1);
        gp.add(this.colonne, 1, 1);
        gp.add(lBombes, 0,2);
        gp.add(this.nbrBombe, 1, 2);
        gp.add(lslider, 0, 3);
        gp.add(this.slidertaille, 1, 3);
        gp.add(appliquer,1 ,5);
        gp.add(this.labelTaille,2,3);
        gp.add(erreur, 1, 6,2,1);

        return gp;
    }


    public void setLabel(Double nouvelleValeur) {
        this.labelTaille.setText("" +nouvelleValeur.intValue());
    }
    public void setElement(int nbligne, int nbColonne , int nbBombe) {
        this.choixligne = nbligne;
        this.choixcolonne = nbColonne;
        this.choixnbrbombe = nbBombe;
        
        
    }
    public String getLigne() {
        return this.ligne.getText();
    }
    public String getLabelSlider() {
        return this.labelTaille.getText();
    }

    public String getNbrBombe() {
        return  this.nbrBombe.getText();
    }


    public String getColonne() {
        return  this.colonne.getText();
    }
    public void setErreur(String message){
        this.erreur.setText(message);
    }

    public void nouveauPlateau(){

        try{
            int lignes = Integer.valueOf(this.getParameters().getRaw().get(0));
            int colonnes = Integer.valueOf(this.getParameters().getRaw().get(1));
            int nbBombes = Integer.valueOf(this.getParameters().getRaw().get(2)); 
            this.lePlateau = new Grille(lignes, colonnes, nbBombes);
        }
        catch(Exception e){
            if(choixligne == null)
            this.lePlateau = new Grille(5, 5, 4);
            else
            this.lePlateau = new Grille(choixligne, choixcolonne , choixnbrbombe);
        }

    }

    public void rejouer() {

        this.grille = new GridPane();
        this.nouveauPlateau();
        this.grille.setHgap(1);
        this.grille.setVgap(1);
        this.grille.setAlignment(Pos.CENTER);
        poseBouton();
        this.jeu.setCenter(this.grille);
        this.jeu.setBottom(this.poseInfo());
        
    }

    public Grille getPlateau() {
        return this.lePlateau;
    }

    public void setCouleur(String value) {
        String couleur = value.substring(2,value.toString().length()-2);
        this.top.setStyle("-fx-background-color: #" + couleur + ";");
        this.couleurBgTop = Color.web("#" + couleur);
    }

    public void changeJeuPendu(){
        this.pendu.init();
        this.pendu.start(this.affichage);
    }
    public Alert popUpChangeDeJeu(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION," Êtes-vous de vouloir changer de jeu ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
}
