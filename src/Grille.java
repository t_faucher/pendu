import java.lang.Math;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Grille{

    private int nbLignes;
    private int nbColonnes;
    private int nbBombes;
    private List<List<Case>> grille;
    
    public Grille(int nbLignes, int nbColonnes, int nbBombes){

        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.nbBombes = nbBombes;
        this.grille = creerPlateau(nbLignes,nbColonnes);
        this.poserBombe(this.grille, nbBombes);
        this.mettreCaseVoisine();

    }
    /** 
     * Renvoie le nombre le nombre de ligne de la grille
     * @return int
     */
    public int getNombreDeLignes(){
        return this.nbLignes;
    }
    /** 
     * Renvoie le nombre de colonne
     * @return int
     */
    public int getNombreDeColonnes(){
        return this.nbColonnes;
    }
    /** 
     * Renvoie le nombre de bombe de la grille
     * @return int
     */
    public int getNombreDeBombes(){
        return this.nbBombes;
    }
    /** 
     * Renvoie la case associer à la position
     * passer en paramètre (i,y) dans la grille 
     * @return int
     */
    public Case getCase(int i, int y){
        return this.grille.get(i).get(y);
    }
    /** 
     * Renvoie la grille 
     * @return int
     */
    public List<List<Case>> getGrille(){
        return this.grille;
    
    }
    /** 
     * Renvoie le nombre de cases révélés, c'est-à-dire 
     * le nombre de case que le joueur voit
     * @return int
     */
    public int getNombreDeCasesRevelees(){
        int caseRevele = 0;
        for(int i = 0; i<this.getNombreDeLignes(); ++i){
            for(int y = 0; y<this.getNombreDeColonnes(); ++y){
                if(getCase(i, y).estRevelee()){
                    caseRevele++;
                }
            }
        }
        return caseRevele;
    }
    /** 
     * Créer le plateau selon le nombre de lignes et  
     * le nombre de colonne passé en paramètre et renvoie le plateau
     * créer sous forme de liste de listes de cases.
     * @param nbLignes
     * @param nbColonnes
     * @return List<List<Case>>
     */
    public List<List<Case>> creerPlateau(int nbLignes, int nbColonnes){
        List<List<Case>> plateau = new ArrayList<>();
        for(int i = 0; i<nbLignes; ++i){
            List<Case> ligne = new ArrayList<>();
            for(int y = 0; y<nbColonnes; ++y){
                ligne.add(new Case());
            }
            plateau.add(ligne);
            
        }
        return plateau;
    }
    /** 
     * Ajoute pour chaque case du plateau, toute ses cases 
     * voisines dans sa liste des cases voisines.
     */
    public void mettreCaseVoisine(){
        for(int ligne = 0; ligne<this.nbLignes ; ++ligne){
            for(int colonne = 0; colonne<this.nbColonnes ; ++colonne){
                for(int descriminant = -1 ; descriminant<2; ++descriminant){
                    for(int descriminant2 = -1; descriminant2<2; ++descriminant2){
                        try{
                            this.grille.get(ligne).get(colonne).ajouteCaseVoisine(this.grille.get(ligne+descriminant).get(colonne+descriminant2));
                        }
                        catch(java.lang.IndexOutOfBoundsException e){
                            continue;
                        }
                    }
                }
            }
        }
    }

    /** 
    * Pose sur le plateau passé en paramètre, le nombre de bombes
    * (nbBombes) passé en paramètre.
    * @param plateau
    * @param nbBombes
    * @return boolean
    */
    public boolean poserBombe(List<List<Case>> plateau , int nbBombes){
        int nbBombePosee = 0;
        while(nbBombePosee != nbBombes){
            int xbombe = (int) (Math.random()*nbLignes);
            int ybombe = (int) (Math.random()*nbColonnes);
            if(xbombe < this.nbLignes && ybombe < this.nbColonnes){
                if(getCase(xbombe,ybombe).ajouteBombe()){
                    nbBombePosee++;
                }   
            }
        }
        return nbBombePosee == nbBombes;
    }
    /** 
     * Renvoie le nombre de case marquées sur la plateau
     * @return int
     */
    public int getNombreDeCasesMarquees(){
        int caseMarque = 0;
        for(int i = 0; i<this.getNombreDeLignes(); ++i){
            for(int y = 0; y<this.getNombreDeColonnes(); ++y){
                if(getCase(i, y).estMarquee()){
                    caseMarque++;
                }
            }
        }
        return caseMarque;
    }
    /** 
     * Renvoie true si le jeu est perdu et false sinon
     * @return boolean
     */
    public boolean estPerdu(){
        for(int i = 0; i<this.getNombreDeLignes(); ++i){
            for(int y = 0; y<this.getNombreDeColonnes(); ++y){
                if(getCase(i, y).estRevelee() && getCase(i, y).estBombe()){
                    return true;
                }
            }
        }
        return false;
    }
    /** 
     * Renvoie true si la partie est gagné et false sinon.
     * @return boolean
     */
    public boolean estGagnee(){
        return getNombreDeCasesRevelees()+nbBombes == this.nbColonnes * this.nbLignes;
    }    
    /** 
    * Permet de découvrir toute les cases du plateau
    */
    public void decouvreToutesLesCases() {
        for(List<Case> ligne : this.grille){
            for(Case uneCase : ligne){
                uneCase.reveler();
            }
        }
    }
    /** 
     * Permet de demander l'action voulu par le joueur 
     * c'est-à-dire le coup voulu et la position choisi.
     * Vérifie aussi que le coup est possible (correct) 
     * @return String
     */
    public String[] demandeActionJoueur(){
        Scanner lecteur = new Scanner(System.in);
        boolean action_bonne = false;
        String [] action_brut = {" ", " " ," "};
        while (!(action_bonne)){
            action_brut = lecteur.nextLine().split(" ");
            try{
                actionCorrect(action_brut);
                action_bonne = true;

            }catch(MessageIncorrect e){
                if(e.getPosition() == -1){
                    System.out.println("Le format du message est incorrect");
                }
                else{
                    System.out.println("Il y un problème à l'indice " + e.getPosition());
                }
            }
            catch(CaseNotFound e){
                System.out.println("Les coordonnées saisies sont incorrectes.");
            }

        }
        return action_brut;
    }
    /** 
     * Permet de vérifier qu'une action est correct, qu'elle est légale
     * @param action_brut
     * @throws MessageIncorrect
     * @throws CaseNotFound
     */
    public void actionCorrect(String [] action_brut) throws MessageIncorrect, CaseNotFound{
        if(action_brut.length != 3){
            throw new MessageIncorrect();
        }
        if(action_brut[0].equals("M") || action_brut[0].equals("R")){
            try{
                Integer.parseInt(action_brut[1]);
            }    
            catch(NumberFormatException e){
                throw new MessageIncorrect(1);
            }
            try{
                Integer.parseInt(action_brut[2]);
            }
            catch(NumberFormatException e){
                throw new MessageIncorrect(2);
            }
            int x = Integer.parseInt(action_brut[1]);
            int y = Integer.parseInt(action_brut[2]);
            try{
                getCase(x, y);
            }
            catch(java.lang.IndexOutOfBoundsException e){
                throw new CaseNotFound();
            }
        }else{
            throw new MessageIncorrect(0);
        }

    }
    /**
     * Permet de réaliser l'action demander par l'utilisateur 
     * soit en marquant une case, soit en la dévoilant  
     * @param action_brute
     * @return boolean
     */
    public void actionJoueur(String[] action_brute){
        Case laCase = this.getCase(Integer.parseInt(action_brute[1]), Integer.parseInt(action_brute[2]));
        if(action_brute[0].equals("M")){
            laCase.marquer();
        }
        else{
            this.decouvreCase(laCase);
        }
    }
    /** Cette méthode permet dans le cas où le joueur dévoile une case qui n'a aucune 
    * bombe voisine (valeur 0 sur le plateau), de découvrir toute les cases voisines à cette case 
    * qui n'ont aussi pas de bombe voisine (cf : https://demineur.org/). 
     * @param ligne
     * @param colonne
     * @return int
     */
    public void decouvreCase(Case uneCase){
        if(uneCase.getNbBombesVoisines() == 0 && !uneCase.estRevelee()){ 
            for(Case c : uneCase.getCasesVoisines()){
                uneCase.reveler();
                decouvreCase(c);          
            } 
        }
        uneCase.reveler();        
    }

    public void regenerePlateau(Case laCase){
        while(this.estPerdu() || laCase.getNbBombesVoisines() >0){
            this.resetBombe();
            this.poserBombe(this.grille, nbBombes);
            this.mettreCaseVoisine();
        }
}


    private void resetBombe() {
        for(List<Case> ligne : this.grille){
            for(Case uneCase : ligne){
                uneCase.retireBombe();
                uneCase.retireCaseVoisine();
            }
        }
    }
    /** 
     * Permet de lancer le jeu et de modéliser la partie tour après tour
     */
    public void lanceJeu(){
        boolean fini = false;
        while(!(fini)){
            this.affiche();
            System.out.println("Entrer une instruction de la forme R 3 2 ou M 3 2 \npour Révéler/Marquer la case à la ligne 3 et à la colonne 2");
            String [] action_brut = this.demandeActionJoueur(); 
            Case laCase = getCase(Integer.parseInt(action_brut[1]), Integer.parseInt(action_brut[2]));
            if(this.getNombreDeCasesRevelees() == 0){
                while(laCase.estBombe() || laCase.getNbBombesVoisines() != 0){
                    this.grille = creerPlateau(this.nbLignes, this.nbColonnes);
                    this.poserBombe(this.grille, nbBombes);
                    this.mettreCaseVoisine();
                    laCase = getCase(Integer.parseInt(action_brut[1]), Integer.parseInt(action_brut[2]));
            
                }
            }
            actionJoueur(action_brut);
            if(estPerdu()){
                fini = true;
                this.decouvreToutesLesCases();
                this.affiche();
                System.out.println("Perdu, tu es tombé(e) sur une bombe !");
            }
            if(estGagnee()){
                fini = true;
                this.decouvreToutesLesCases();
                this.affiche();
                System.out.println("Félicitation tu as réussi ce démineur !");
                }
            }
        if(rejouer()){
            lanceJeu();
        }
    }
    /** Cette méthode permet de demander si le joueur souhaite rejouer une partie
     * @return boolean
     */
    public boolean rejouer() {

        Scanner lecteur = new Scanner(System.in);
        System.out.println("Voulez-vous rejouer (O/N)");
        while(true){
            String action_brut = lecteur.next();
            if(action_brut.equals("O")){
                this.grille = creerPlateau(nbLignes, nbColonnes);
                this.poserBombe(this.grille, nbBombes);
                this.mettreCaseVoisine();
                return true;
                
            }
            else if(action_brut.equals("N")){
                System.out.println("Merci d'avoir joué");
                return false;
            }
            else{
                System.out.println("Saisie Incorrecte");
                continue;
            }
        }
    }

    /** Cette méthode permet d'afficher le plateau 
     */
    public void affiche(){
        String affichage = "    ";
        for(int colonne = 0; colonne<this.nbColonnes; ++colonne){
            if(!(colonne>=10)){
                affichage += " "+ colonne + "  ";
            }else{
            affichage += " "+ colonne + " ";
            }
        }
        affichage += "\n" + "   ┌";
        for(int colonne = 0; colonne<nbColonnes-1; ++colonne){
            affichage += "───┬";
        }
        affichage += "───┐"+"\n";
        for(int ligne = 0; ligne<this.nbLignes;++ligne){
            affichage += "" + ligne + "  │";
            for(int colonne = 0; colonne<this.nbColonnes; ++colonne){
            	if(getCase(ligne,colonne).estRevelee()){
                	affichage += " " +getCase(ligne, colonne).getAffichage() + " │";
                }else if (getCase(ligne, colonne).estMarquee()){
                    affichage +=getCase(ligne, colonne).getAffichage() + " │";
                }
                else{
                    affichage +=  "   │";
                }
            }
            affichage += "\n";
            if(ligne==this.nbLignes-1){
                affichage += "   └";
                for(int i = 0; i<this.nbColonnes-1; ++i){
                    affichage += "───┴";
                }
                affichage += "───┘";
            }else{
                affichage +="   ├";
                for(int i = 0; i<this.nbColonnes-1; ++i){
                    affichage += "───┼";
                }
                affichage += "───┤\n";
            }
        }
        System.out.println(affichage);
    }
    public void decouvreToutesLesCasesBombes() {

        for(List<Case> ligne : this.grille){
            for(Case uneCase : ligne){
                if(uneCase.estBombe())
                    uneCase.reveler();
                
            }
        }
        
    }

}
