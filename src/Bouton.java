import java.util.Arrays;
import java.util.List;

import javafx.scene.control.Button; 
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;;

public class Bouton extends Button{
    
    private Case laCase;
    private List<Color> couleur;
    public Bouton(Case laCase,double taille){
        super();
        this.couleur = Arrays.asList(Color.WHITE,Color.DODGERBLUE, Color.DARKGREEN,Color.RED ,Color.PURPLE,Color.MAROON, Color.DARKGOLDENROD, Color.DARKMAGENTA);
        this.laCase = laCase;
        this.setPrefWidth(taille);
        this.setPrefHeight(taille);
        if (laCase.estRevelee() && laCase.estBombe())
            ajouteImage("bombe.png");
        else if (laCase.estRevelee() && laCase.estBombe())
            ajouteImage("bombe.png");
        
    }
 
    private void ajouteImage(String fichierImage){
        try{

                Image image = new Image(fichierImage);
                ImageView iv = new ImageView();
                iv.setImage(image);
                iv.setFitWidth(20);
                iv.setPreserveRatio(true);
                this.setGraphic(iv);
                this.setText("");
            }
        catch(Exception e){
            this.setText(laCase.getAffichage());
        }
    }
    
    public void maj(){
        this.setText(this.laCase.getAffichage());
        if (this.laCase.estRevelee()){
            this.setGraphic(null);
            

            if(this.laCase.estBombe())
                this.ajouteImage("bombe.png");
               
            this.setDisable(true);
            
            try{
                String num = this.laCase.getAffichage().replace(" ", "");
                int index = Integer.parseInt(num);
                this.setTextFill(this.couleur.get(index));
                if(this.laCase.getAffichage().contains("0")){
                	this.setText("");
                }
                this.setGraphic(null);
                this.setStyle("-fx-opacity:0.5; -fx-font-weight:bold;");
            }
            catch(NumberFormatException e){
                
            }

            
        }else{
            this.setDisable(false);
            if(this.laCase.estMarquee()){
                this.ajouteImage("drapeau.png");
                
            }else{
                this.setGraphic(null);
            }
        }
            

    }
}
