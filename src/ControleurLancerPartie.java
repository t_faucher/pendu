
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;

import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une partie
 */
public class ControleurLancerPartie implements EventHandler<ActionEvent> {
    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     **/
    private Pendu vuePendu;

    /**
     * @param modelePendu modèle du jeu
     * @param p vue du jeu
     */
    public ControleurLancerPartie(MotMystere modelePendu, Pendu vuePendu) {
        this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;
    }

    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y a pas une partie en cours
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        // A implémenter
        Button boutton = (Button) actionEvent.getSource();
        if(boutton.getText().contains("Lancer")){
            RadioButton rd = (RadioButton) this.vuePendu.getToggle();
            int diff = 0;
            switch(rd.getText()){
                case "Facile":
                    diff = 0;
                    break;
                case "Médium":
                    diff = 1;
                    break;
                case "Difficile":
                    diff = 2;
                    break;
                case "Expert":
                    diff = 3;
                    break;
            }
            this.modelePendu.setNiveau(diff);

             
            this.vuePendu.lancePartie();
            this.vuePendu.majAffichage();
        }
        if(boutton.getText().contains("Nouveau mot")){
            if(!(this.modelePendu.gagne() || this.modelePendu.perdu())){
                Optional<ButtonType> reponse = this.vuePendu.popUpPartieEnCours().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            // si la réponse est oui
                if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                    this.vuePendu.lancePartie();
                    this.vuePendu.majAffichage();
                }
            }else{
                this.vuePendu.lancePartie();
                    this.vuePendu.majAffichage();
            }
        }
      
        
    }
}
